import React,{ Component } from 'react';
import './App.css';
import Person from './Person/Person'
class  App extends Component {
  render(){
    return (
      <div className="App">
        <h1>Hi, i'm a react app</h1>
        <Person name="Maria" age="23" />
        <Person name="João" age="26"/>
      </div>
    );
  }
}

export default App;
